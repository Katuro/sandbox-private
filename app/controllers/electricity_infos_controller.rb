class ElectricityInfosController < ApplicationController
  def index
    @electricity_infos = ElectricityInfo.order(:created_at).paginate(page: params[:page], per_page: 10)
  end

  def create
    file_data = params[:uploaded_file].try(:[], :xml)
    result = ElectricityInfo.upload_xml_file(file_data)
    flash[:success] = result[:success] if result[:success].present?
    flash[:danger] = result[:errors] if result[:errors].present?
    redirect_to new_electricity_info_path and return
  end
end
