class ElectricityInfo < ApplicationRecord
  validates_presence_of :pdl_id
  validates_presence_of :begin_at
  validates_presence_of :end_at
  validates_presence_of :index_start
  validates_presence_of :index_end
  validates_presence_of :consumption

  def self.upload_xml_file file_data
    result = {success: "", errors: ""}
    result[:errors] << "Must be a XML file." and return result unless file_data.respond_to?(:read) and file_data.content_type == 'text/xml'

    electricity_info = {}
    xml_contents = file_data.read
    hash = Hash.from_xml(xml_contents.gsub("\n", ""))
    clients = hash["Index_C5"]["Corps_de_fichier_par_PDL"] rescue nil

    if clients.nil?
      result[:errors] << "Your file doesn't contain any clients well formated."
    else
      clients.each_with_index do |client, index|
        electricity_info[:pdl_id] = client["Identifiant_Stable_PDL"].to_i
        electricity_info[:begin_at] = client["Situation_Contrat"]["Date_Debut_Consommation"] rescue nil
        electricity_info[:end_at] = client["Situation_Contrat"]["Date_Fin_Consommation"] rescue nil
        electricity_info[:index_start] = client["Situation_Contrat"]["Compteur"]["Periode"]["Donnees_Cadran"]["Index_Debut_Consommation"] rescue nil
        electricity_info[:index_end] = (client["Situation_Contrat"]["Compteur"]["Periode"]["Donnees_Cadran"]["Index_A_Facturer"] rescue nil) || '0'
        electricity_info[:consumption] = client["Situation_Contrat"]["Compteur"]["Periode"]["Donnees_Cadran"]["Consommation_Cadran"] rescue nil

        result[:errors] << "The #{index}th client is missing some required informations." and next if electricity_info.values.include? nil
        ei = ElectricityInfo.create(electricity_info)
        result[:success] = "All client(s) without error(s) have been uploaded succesfully." if ei.id
      end
    end

    result
  end
end
