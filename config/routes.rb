Rails.application.routes.draw do
  root 'home#index'

  resources :electricity_infos, only: [:index, :create, :new]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
