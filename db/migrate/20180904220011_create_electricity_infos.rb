class CreateElectricityInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :electricity_infos do |t|
      t.string  :pdl_id
      t.date    :begin_at
      t.date    :end_at
      t.string  :index_start
      t.string  :index_end
      t.integer :consumption

      t.timestamps
    end
  end
end
