require 'test_helper'

class ElectricityInfoControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get electricity_infos_index_url
    assert_response :success
  end

  test "should get show" do
    get electricity_infos_show_url
    assert_response :success
  end

end
